class reverse:
    def __init__(self, num=1):
        self.num = num
        self.rev = 0

    def reversDigits(self):
        n = self.num
        self.rev = 0
        while (n > 0):
            a = n % 10
            self.rev = self.rev * 10 + a
            n = n // 10
        return self.rev

    def add_num(self, s):
        self.num = s

    def readFromFile(self, filename):
        file = open(filename, "r")
        line = file.readline()
        return line


if __name__ == "__main__":
    obj1 = reverse(89)
    obj2 = reverse(7554000)
    print( obj1.reversDigits())
    print(obj2.reversDigits())

